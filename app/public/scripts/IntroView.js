this.IntroView = (function(){

	var OFFICES = {
		four: {
			address: '516 Clyde Avenue\nMountain View, CA 94043',
			mapsUrl: 'https://maps.google.com/maps?q=Blue+Jeans+Network&hl=en&ll=37.398528,-122.046776&spn=0.270018,0.528374&cid=14115605422088510097&gl=US&t=m&z=12',
			yelpId: 'bluejeans-mountain-view'
		},
		six: {
			address: '625 2nd Street\nSuite 104\nSan Francisco, CA 94107',
			mapsUrl: 'https://maps.google.com/maps?q=625+2nd+Street+%23104,+San+Francisco,+CA+94107&hl=en&ll=37.781366,-122.391386&spn=0.066887,0.132093&sll=37.781358,-122.391386&sspn=0.066887,0.132093&hnear=625+2nd+St+%23104,+San+Francisco,+California+94107&t=m&z=14&iwloc=A',
			yelpId: null
		}
	};
	OFFICES.mv2 = OFFICES.mv;

	var IntroView = Backbone.View.extend({

		className: 'intro',

		initialize: function(){
			_.bindAll(this);
		},

		render: function(){
			if(this.$el.is(':empty')){
				this.$el.addClass(floorplanParams.officeId);
				var office = OFFICES[floorplanParams.officeId];

				this.$el.append($('<h2>', { text: 'Blue Jeans' }));

				if(office.address){
					var addressEl = $('<h3>', { class: 'address' });
					if(office.mapsUrl){
						addressEl.append($('<a>', {
							text   : office.address,
							title  : "View in Google Maps",
							href   : office.mapsUrl,
							target : '_blank'
						}));
					} else {
						addressEl.text(office.address);
					}
					this.$el.append(addressEl);
				}

				if(office.yelpId){
					var ratingEl = $('<div>', { class: 'rating' })
						.click(function(){
							window.open('http://www.yelp.com/biz/'+office.yelpId);
						});
					this.$el.append(ratingEl);

					this.renderRating(office.yelpId);
				}
			}

			return this.el;
		},

		renderRating: function(yelpId){
			yelp.getRating(yelpId)
				.then(_.bind(function(rating){
					this.$('.rating')
						.css('background-position', '-2px '+(-3 - 18*2*(rating.stars-.5))+'px')
						.attr('title', rating.stars + ' stars on Yelp\n('+rating.reviews+' reviews)');
				}, this));
		}
	});

	return IntroView;

})();
